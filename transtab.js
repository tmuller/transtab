const Fct = {

  lsProperty: 'userInfo',
  dataToStore: {user: "Somebody", loginDate: "2021-01-12T13:45:41.000Z"},
  instanceId: Math.trunc(Math.random() * 100000),

  init() {
    window.addEventListener('storage', this.onLocalStorageEvent.bind(this));
    document.getElementById('source-id').innerText = this.instanceId;
    console.log('instanceId', this.instanceId);
  },

  onLocalStorageEvent(authData) {
    console.log('Event::loggedIn?', authData);
    const isLoggedIn = !!(authData && authData.newValue);
    this.updateUI(isLoggedIn);
  },

  triggerBtnClickEvent() {
    const loggedIn = this.setNewState();
    this.updateUI(loggedIn)
  },

  setNewState() {
    const storedData = window.localStorage[this.lsProperty];
    const newValues = {loginDate: new Date().toISOString(), sourceId: this.instanceId};
    const dataToSend = {...this.dataToStore, ...newValues};

    if (storedData) delete window.localStorage[this.lsProperty];
    else {
      window.localStorage[this.lsProperty] = JSON.stringify(dataToSend);
    }
    return !!window.localStorage[this.lsProperty];
  },

  // Display functionality

  updateUI: function (isLoggedIn) {
    document.getElementById('statusOutput').innerText = isLoggedIn ? 'Yes' : 'No';
    document.getElementById('json-output').innerText = this.formatJson();
  },

  formatJson() {
    const currentLsValue = window.localStorage[this.lsProperty];
    const jsonObj = currentLsValue && JSON.parse(currentLsValue);
    return JSON.stringify(jsonObj, null, 3);
  },

};
